import React from 'react';
import './App.css';
import LeftContainer from './components/LeftContainer';
import RightContainer from './components/RightContainer';
import BottomContainer from './components/BottomContainer';
import MainContainer from './components/MainContainer';

import music1 from './assets/songs/Figure Me Out.mp3';
import music2 from './assets/songs/Im Not Afraid of Anything.mp3';
import music3 from './assets/songs/Piano Church.mp3';
import music4 from './assets/songs/Waltz of Four Left Feet.mp3';
import music5 from './assets/songs/Where Im At.mp3';
import music6 from './assets/songs/You Make My Dreams.mp3';
import musiclist from './assets/songs/sample-music.js';

class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      play: false,
      playAction: '',
      track: '',
      musicArray: {},
      nowPlaying: {},
      previousSong: {},
      nextSong: {}
    }
  }

  componentWillMount = async () => {
    let musicArray = this.state.musicArray;
    musicArray = musiclist;
    await this.setState({ musicArray });
  }

  playMusic = async (action, file, nowPlaying) => {
    let musicarray = this.state.musicArray;
    console.log('passed action: ', action);
    console.log('passed file: ', file);
    await this.setState({ play: action });
    await this.setState({ nowPlaying });
    console.log('NOW PLAYING: ', this.state.nowPlaying);
    console.log('MUSIC LISSSSST: ', musicarray);

    let play = 'PLAYING';
    let pause = 'PAUSED';
    let playAction = this.state.playAction;

    if (this.state.play) {
      playAction = play;
      await this.setState({ playAction });
      console.log('ANO BAAAAAAAA? ', file);

      if (file === "../assets/songs/Figure Me Out.mp3") {
        this.setState({ track: music1, previousSong: music6, nextSong: music2 });
      } else if (file === "../assets/songs/Im Not Afraid of Anything.mp3") {
        this.setState({ track: music2, previousSong: music1, nextSong: music3 });
      } else if (file === "../assets/songs/Piano Church.mp3") {
        this.setState({ track: music3, previousSong: music2, nextSong: music4 });
      } else if (file === "../assets/songs/Waltz of Four Left Feet.mp3") {
        this.setState({ track: music4, previousSong: music3, nextSong: music5 });
      } else if (file === "../assets/songs/Where Im At.mp3") {
        this.setState({ track: music5, previousSong: music4, nextSong: music6 });
      } else if (file === "../assets/songs/You Make My Dreams.mp3") {
        this.setState({ track: music6, previousSong: music5, nextSong: music1 });
      } else {
        console.log('');
      }

    } else {
      playAction = pause;
      await this.setState({ playAction });
    }
  }

  previousAction = (musicStatusProps, filePath, nowPlaying) => {
    console.log('previous action');
  }

  render() {
    return (
      <div className="App">

        <LeftContainer className="left-container" />

        <MainContainer musiclist={this.state.musicArray} musicAction={this.playMusic} playAction={this.state.playAction} track={this.state.track} play={this.state.play} className="main-container" />

        <RightContainer className="right-container" />

        <BottomContainer className="bottom-container" play={this.state.play} nowPlaying={this.state.nowPlaying} musicAction={this.playMusic} nextSong={this.state.nextSong} previousSong={this.state.previousSong} previousAction={this.previousAction} />

      </div>
    );
  }
}

export default App;
