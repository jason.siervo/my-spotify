import React, { Component } from 'react';
import PlayButton from '../assets/images/play.png';
import PauseButton from '../assets/images/pause.png';
import Sound from 'react-sound';

class MusicItem extends Component {

    constructor(props) {
        super(props)
        this.state = {
            currentTrack: ''
        }
    }

    handleClick = () => {
        let filePath = '';
        filePath = this.props.trackInfo.filePath;
        console.log('item clicked: ', filePath);
    }

    tester = async () => {
        let filePath = '';
        filePath = this.props.trackInfo.filePath;
        let musicStatusProps = this.props.play;
        let currentTrackCopy = this.state.currentTrack;
        let nowPlaying = this.props.trackInfo;

        if (this.state.currentTrack === '') {
            console.log('new song or another music is played');
            musicStatusProps = true;
            currentTrackCopy = filePath;

            await this.setState({ currentTrack: currentTrackCopy });
            this.props.musicAction(musicStatusProps, filePath, nowPlaying);

        } else if (this.state.currentTrack === this.props.trackInfo.filePath) {
            console.log('SAME SONG');

            if (this.props.play) {
                musicStatusProps = false;

                this.props.musicAction(musicStatusProps, filePath, nowPlaying);
            } else {
                musicStatusProps = true;

                this.props.musicAction(musicStatusProps, filePath, nowPlaying);
            }
        }
    }

    render() {

        let { trackInfo, musicAction, play } = this.props;
        return (
            <tr style={{ marginTop: 100 }} >
                <td>
                    {
                        play ? (
                            <div onClick={this.tester.bind()}>
                                <img alt='pause button' src={PauseButton} />
                            </div>
                        ) : (
                                <div onClick={this.tester.bind()}>
                                    <img alt='play button' src={PlayButton} />
                                </div>
                            )
                    }
                </td>
                <td>{trackInfo.title}</td>
                <td>{trackInfo.artist}</td>
                <td>{trackInfo.album}</td>
                <td>{trackInfo.added}</td>
            </tr>
        )
    }
}

const position = {
    position: 'absolute',
    backgroundColor: '	#212121',
    top: 540,
    left: 0,
    width: '100%',
    height: 80,
}


export default MusicItem;