import React, { Component } from 'react';
import playlistImage from '../assets/images/playlistImage.png';
// import music1 from '../assets/songs/Figure Me Out.mp3';
// import music2 from '../assets/songs/Im Not Afraid of Anything.mp3';
// import music3 from '../assets/songs/Piano Church.mp3';
// import music4 from '../assets/songs/Waltz of Four Left Feet.mp3';
// import music5 from '../assets/songs/Where Im At.mp3';
// import music6 from '../assets/songs/You Make My Dreams.mp3';
// import PlayButton from '../assets/images/play.png';
// import PauseButton from '../assets/images/pause.png';
import Sound from 'react-sound';
import MusicItem from './MusicItem';

class MainContainer extends Component {

    constructor(props) {
        super(props)
        this.state = {
            play: false,
            playAction: '',
            track: '',
            musicArray: {},
        }
    }

    redirectFunction = async (action, file, nowPlaying) => {
        this.props.musicAction(action, file, nowPlaying);
    }

    audio = new Audio(this.props.url)

    render() {

        let { musiclist } = this.props;


        let { musicArray } = this.state;
        return (
            <div className="container" style={containerStyle}>
                <div style={contentContainer}>
                    <div className="header">
                        <div row={1} style={textareaStyle} />
                    </div>

                    <div className="playlistHeader" style={playlistHeaderStyle}>
                        <img alt='Playlist' src={playlistImage} style={playlistImageStyle} />

                        <text style={{ position: 'absolute', left: 220, top: 55 }}>PLAYLIST</text><br></br>
                        <text style={{ position: 'absolute', left: 220, top: 80, fontSize: 40 }}>Grew Up At Midnight</text><br></br>
                        <text style={{ position: 'absolute', left: 220, top: 140 }}>Created by Jason Siervo - 10 songs, 42 minutes</text>

                        <div style={playButtonGreen}>
                            <text style={{ fontSize: 15, color: 'white', textAlign: 'center', top: 5 }}>PLAY</text>
                        </div>
                    </div>

                    <div className="songContainer" style={songContainerStyle}>
                        <table style={songTableStyle}>
                            <tbody>
                                <tr>
                                    <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                    <th>TITLE</th>
                                    <th>ARTIST</th>
                                    <th>ALBUM</th>
                                    <th>DATE ADDED</th>
                                </tr>

                                <Sound
                                    url={this.props.track}
                                    playStatus={this.props.playAction}
                                    playFromPosition={300}
                                    onLoading={this.handleSongLoading}
                                    onPlaying={this.handleSongPlaying}
                                    onFinishedPlaying={this.handleSongFinishedPlaying}
                                />

                                {
                                    Object.keys(this.props.musiclist).map((key, index) => {
                                        return (
                                            <MusicItem key={index} trackInfo={this.props.musiclist[key]} musicAction={this.redirectFunction} play={this.props.play} />
                                        );
                                    })
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

const containerStyle = {
    position: 'absolute',
    backgroundColor: '	#535353',
    top: 0,
    left: 270,
    width: 826,
    height: 550,
    overflow: 'auto',
}

const playlistImageStyle = {
    position: 'absolute',
    left: 0,
    top: 0,
    height: 200
}

const contentContainer = {
    marginTop: 10,
    marginLeft: 25,
    marginRight: 25,
    marginBottom: 20,
}

const playlistHeaderStyle = {
    marginTop: 20,
    width: 770,
    position: 'relative'
}

const textareaStyle = {
    borderRadius: 20,
    width: 150,
    height: 25,
    backgroundColor: 'white'
}

const songContainerStyle = {
    backgroundColor: 'gray',
    marginTop: 180,
}

const playButtonGreen = {
    position: 'absolute',
    top: 170,
    left: 220,
    backgroundColor: '#1db954',
    borderRadius: 20,
    width: 90,
    height: 30,
}

const songTableStyle = {
    backgroundColor: '	#535353',
    width: '100%',
}

export default MainContainer;