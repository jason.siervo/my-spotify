import React, { Component } from 'react';
import playlistImage from '../assets/images/playlistImage.png';
import PlayButton from '../assets/images/play.png';
import PauseButton from '../assets/images/pause.png';
import PreviousButton from '../assets/images/previous.png';
import NextButton from '../assets/images/next.png';

class BottomContainer extends Component {

    constructor(props) {
        super(props)
        this.state = {
            currentTrack: ''
        }
    }

    playPause = async (type) => {
        let filePath = '';
        filePath = this.props.nowPlaying.filePath;
        let musicStatusProps = this.props.play;
        let nowPlaying = this.props.nowPlaying;

        if (type === 'play') {
            console.log('new song or another music is played');
            musicStatusProps = true;

            this.props.musicAction(musicStatusProps, filePath, nowPlaying);

        } else if (type === 'pause') {
            console.log('SAME SONG');

            musicStatusProps = false;

            this.props.musicAction(musicStatusProps, filePath, nowPlaying);
        }
    }

    previousButtonAction = () => {
        console.log('previous song to play: ', this.props.previousSong);
        let musicStatusProps = this.props.play;
        musicStatusProps = true;
        let filePath = this.props.previousSong;
        let nowPlaying = this.props.previousSong;

        this.props.previousAction(musicStatusProps, filePath, nowPlaying);
    }

    nextButtonAction = () => {

    }

    render() {

        let { nowPlaying, play } = this.props;

        return (
            <div className="container" style={position}>
                <img src={playlistImage} style={imageStyle} />
                <div style={SongDetailsStyle}>
                    <text style={{ textAlign: 'left' }}>{nowPlaying.title}</text><br />
                    <text style={{ textAlign: 'left' }}>{nowPlaying.artist}</text>
                </div>

                <div className='playerControls' style={playerControlStyle}>

                    <img onClick={this.previousButtonAction} src={PreviousButton} style={previousButtonStyle} />

                    {
                        play ? (
                            <div onClick={() => { this.playPause('pause') }}>
                                <img alt='pause button' src={PauseButton} />
                            </div>
                        ) : (
                                <div onClick={() => { this.playPause('play') }}>
                                    <img alt='play button' src={PlayButton} />
                                </div>
                            )
                    }
                    <img onClick={this.nextButtonAction} src={NextButton} style={nextButtonStyle} />
                </div>
            </div>
        )
    }
}

const position = {
    position: 'absolute',
    backgroundColor: '	#212121',
    top: 545,
    left: 0,
    width: '100%',
    height: 80,
}

const imageStyle = {
    position: 'absolute',
    top: 12,
    left: 12,
    height: 55
}

const SongDetailsStyle = {
    position: 'absolute',
    left: 80,
    top: 20,
    color: 'white',
    textAlign: 'left'
}

const playerControlStyle = {
    marginTop: 10
}

const previousButtonStyle = {
    position: 'absolute',
    left: 600
}

const nextButtonStyle = {
    position: 'absolute',
    right: 600,
    top: 10
}

export default BottomContainer;