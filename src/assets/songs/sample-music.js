// This is just some sample data so you don't have to think of your own!
const songs = {
    1: {
        title: "Figure Me Out",
        artist: "Jason Siervo",
        album: "Dear Evan Hansen",
        added: "2017-01-01",
        duration: "available",
        filePath: "../assets/songs/Figure Me Out.mp3"
    },
    2: {
        title: "I'm Not Afraid of Anything",
        artist: "Colton Ryan",
        album: "Phantom of the Opera",
        added: "2017-01-11",
        duration: "available",
        filePath: "../assets/songs/Im Not Afraid of Anything.mp3"
    },
    3: {
        title: "Piano Church",
        artist: "Sharon Van Etten",
        album: "Mormons",
        added: "2017-12-01",
        duration: "available",
        filePath: "../assets/songs/Piano Church.mp3"
    },
    4: {
        title: "Waltz of Four Left Feet",
        artist: "Shirebound and Busking",
        album: "In The Lonely Hour",
        added: "2017-01-01",
        duration: "available",
        filePath: "../assets/songs/Waltz of Four Left Feet.mp3"
    },
    5: {
        title: "Where I'm At",
        artist: "Colton Ryan",
        album: "Defying Gravity",
        added: "2017-01-01",
        duration: "available",
        filePath: "../assets/songs/Where Im At.mp3"
    },
    6: {
        title: "You Make My Dreams",
        artist: "Daryl Hall & John Oates",
        album: "Colton Ryan",
        added: "2017-01-01",
        duration: "available",
        filePath: "../assets/songs/You Make My Dreams.mp3"
    },
};

export default songs;
